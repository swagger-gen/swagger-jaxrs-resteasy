# Swagger jaxrs-resteasy generated server

```
java -jar swagger-codegen-cli-2.2.1.jar generate -i swagger.json -l jaxrs-resteasy
```

Generálás utáni teendők:
* Megfelelő resteasy verzió beállítása a pom-ban (3.0.19.Final a provided scope miatt)
* jaxrs-api eltávolítása (friss verzióban már nem létezik ilyen)

Előnyök:
* Jboss alapból a resteasy-t támogatja (nem kell külön modult telepíteni, vagy a war-ba csomagolni)
* szépen struktúrált kód (Api, abstract ApiService, ApiServiceImpl, ApiServiceFactory)

Hátrányok:
* válasz vázát nem generálja le, mindenhol egy ApiResponseMessage-el tér vissza alapból
```
    @POST

    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response login(LoginDTO loginDTO, @Context SecurityContext securityContext)
            throws NotFoundException {
        return delegate.login(loginDTO, securityContext);
    }
```
* pom-ban sok függőség alapból (mindne lehetséges függőséget belegenerál)
