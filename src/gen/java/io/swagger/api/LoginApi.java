package io.swagger.api;

import io.swagger.model.*;
import io.swagger.api.LoginApiService;
import io.swagger.api.factories.LoginApiServiceFactory;

import io.swagger.model.LoginDTO;
import io.swagger.model.LoginResponseDTO;

import java.util.List;

import io.swagger.api.NotFoundException;

import java.io.InputStream;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.*;

@Path("/login")
@Consumes({"application/json"})
@Produces({"application/json"})
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2017-03-21T13:16:46.332+01:00")
public class LoginApi {
    private final LoginApiService delegate = LoginApiServiceFactory.getLoginApi();

    @POST

    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response login(LoginDTO loginDTO, @Context SecurityContext securityContext)
            throws NotFoundException {
        return delegate.login(loginDTO, securityContext);
    }
}
