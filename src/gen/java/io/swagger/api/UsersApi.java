package io.swagger.api;

import io.swagger.model.*;
import io.swagger.api.UsersApiService;
import io.swagger.api.factories.UsersApiServiceFactory;

import io.swagger.model.UserDTO;

import java.util.List;

import io.swagger.api.NotFoundException;

import java.io.InputStream;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.*;

@Path("/users")
@Consumes({"application/json"})
@Produces({"application/json"})
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2017-03-21T13:16:46.332+01:00")
public class UsersApi {
    private final UsersApiService delegate = UsersApiServiceFactory.getUsersApi();

    @GET

    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response getUsers(@Context SecurityContext securityContext)
            throws NotFoundException {
        return delegate.getUsers(securityContext);
    }
}
