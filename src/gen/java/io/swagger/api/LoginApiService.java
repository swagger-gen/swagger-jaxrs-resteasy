package io.swagger.api;

import io.swagger.api.*;
import io.swagger.model.*;


import io.swagger.model.LoginDTO;
import io.swagger.model.LoginResponseDTO;

import java.util.List;

import io.swagger.api.NotFoundException;

import java.io.InputStream;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2017-03-21T13:16:46.332+01:00")
public abstract class LoginApiService {
    public abstract Response login(LoginDTO loginDTO, SecurityContext securityContext)
            throws NotFoundException;
}
