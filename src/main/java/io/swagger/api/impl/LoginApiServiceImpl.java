package io.swagger.api.impl;

import io.swagger.api.LoginApiService;
import io.swagger.api.NotFoundException;
import io.swagger.model.LoginDTO;
import io.swagger.model.LoginResponseDTO;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2017-03-21T13:16:46.332+01:00")
public class LoginApiServiceImpl extends LoginApiService {
    @Override
    public Response login(LoginDTO loginDTO, SecurityContext securityContext)
            throws NotFoundException {
        LoginResponseDTO res = new LoginResponseDTO();
        res.setName(loginDTO.getUsername());
        res.setToken("dsadasdafda");
        return Response.accepted().entity(res).build();
    }
}
