package io.swagger.api.impl;

import io.swagger.api.NotFoundException;
import io.swagger.api.UsersApiService;
import io.swagger.model.UserDTO;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.ArrayList;
import java.util.List;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2017-03-21T13:16:46.332+01:00")
public class UsersApiServiceImpl extends UsersApiService {
    @Override
    public Response getUsers(SecurityContext securityContext)
            throws NotFoundException {

        List<UserDTO> users = new ArrayList<UserDTO>();
        for (int i = 0; i < 10; i++) {
            UserDTO user = new UserDTO();
            user.setId(i);
            user.setName("Teszt Elek " + i);
            users.add(user);
        }

        return Response.ok().entity(users.toArray(new UserDTO[0])).build();
    }
}
